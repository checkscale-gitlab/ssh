# Docker SSH image

A tiny (less than 15MiB) docker image including ssh and scp. Useful for ci deployments which require ssh.

## Usage

Don't forget to set the variables "SSH_PRIVATE_KEY" and "SSH_KNOWN_HOSTS" under [Settings > CI/CD > Variables]

```yaml
deploy:
    image: $CI_REGISTRY/meenzen/ssh:latest
    stage: deploy
    script:
        - eval $(ssh-agent -s)
        - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
        - mkdir -p ~/.ssh
        - chmod 700 ~/.ssh
        - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
        - chmod 644 ~/.ssh/known_hosts
        - ssh root@example.com "echo I'm running via SSH!"
```
